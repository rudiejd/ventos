#include <unity.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <machine.h>

using namespace VentOS;

void test_setup() {
  MachineController mc;
  TEST_ASSERT_EQUAL(true, mc.setup());
}

void test_run_fails_without_being_initialized() {
  MachineController mc;
  TEST_ASSERT_EQUAL(false, mc.run(0));
}

void test_boot_machine() {
  MachineController mc;
  MachineConfig config = {
    "001",
    "test model",
    "test manufacturer",
    "test supplier",
    "001",
    {
        {'B', false, 1234 },
        {'W', false, 2468 },
        {'E', false, 1357 }
    },
    {
        {'D', 'I', 0, 0 },
        {'D', 'E', 0, 0 },
        {'F', 'I', 0, 1 },
        {'F', 'E', 0, 1 },
        {'O', 'I', 0, 2 },
        {'C', 'E', 0, 3 }
    },
    {
        {'S', 'I', 0, 0 },
        {'S', 'E', 0, 0 }
    },
    { 0, 480, 320, 30, true, 0 },
    {
        {'P', 0, 34},
        {'S', 1, 26},
        {'E', 2, 32},
        {'P', 3, 33}
    },
    {
        {'P', 0, 28 },
        {'L', 1, 24 },
        {'L', 2, 22 },
        {'L', 3, 21 }
    }
  };
  TEST_ASSERT_EQUAL(MachineSuccess, mc.bootMachine(config));
}

void test_setup_machine() {
  MachineController mc;
  TEST_ASSERT_EQUAL(MachineSuccess, mc.setupMachine(true));
}

void test_start_machine_auto_mode() {
  MachineController mc;
  TEST_ASSERT_EQUAL(MachineSuccess, mc.startMachine());
}


void process() {
  UNITY_BEGIN();
  RUN_TEST(test_setup);
  RUN_TEST(test_run_fails_without_being_initialized);
  RUN_TEST(test_boot_machine);
  RUN_TEST(test_setup_machine);
  RUN_TEST(test_start_machine_auto_mode);
  UNITY_END();
}

#ifdef ARDUINO
#include <Arduino.h>
void setup() {
    // NOTE!!! Wait for >2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    delay(2000);
    process();
}
void loop() {
    //
}
#else
int main(int argc, char **argv) {
    process();
    return 0;
}
#endif
