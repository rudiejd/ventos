#include <unity.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <controlAPI.h>
#include <controller.h>

#ifdef INCLUDE_UNO_MOCK_DRIVE
#include <drive_simulator_uno.h>
#else
#include <drive_simulator.h>
#endif

using namespace VentOS;

Drive d;
struct VentState v;


#ifdef ARDUINO
#include <Arduino.h>
#else
#include <iostream>
#endif

#include <controller_task_PCV.h>


// We need this only for the call to set_ventilator_state, which
// we are actually not using here!!!
#ifdef INCLUDE_VENTMON_CONFIG
#include <vos_config_ventmon.h>
#define FOUND_CONFIG
#endif

#ifdef INCLUDE_MOCK_CONFIG
#include <vos_config_mock.h>
#define FOUND_CONFIG
#endif

#define MAX_TASKS 2

int num_tasks = 0;
#define NUM_BREATHS_TO_TEST 10
int n = 0;
int iterations = 0;


// NOTE: This is an example of where the "Data Dictionary" would be useful to us
// Quite possibly that should simply be a .h file (at least, one embodiment of it is.)
#define PCV_MODE 0
#define VCV_MODE 1

// We will test two modes...TODO: generalize to testing many modes!

// begin in PCV mode
int mode_under_test = PCV_MODE;
int num_breaths_pcv = 0;
int num_breaths_vcv = 0;

#define DEFAULT_RR_BPM 20.0
#define DEFAULT_E_TO_I  4.0
#define TARGET_PRESSURE_CMH2O 20.0
#define MAX_PRESSURE_CMH2O 45.0
#define TARGET_VOLUME_ML 400
#define MAX_VOLUME_ML 600

struct volatile_ventilator_state vvs;
VentOS::VentState vs;

task TASKS_TO_TEST[MAX_TASKS];
void setup()
{
#ifdef ARDUINO
  Serial.begin(9600);
#else
  set_ventilator_state(&vs);
  vvs.d = &d;
#endif
  vvs.inspiring = 1;
  vvs.mode.mode = PCV_MODE;
  vvs.mode.rr_bpm = DEFAULT_RR_BPM;
  vvs.mode.e_to_i = DEFAULT_E_TO_I;
  vvs.mode.max_pressure_cmH2O = MAX_PRESSURE_CMH2O;
  vvs.md_specifics.pcv.target_pressure_cmH2O = TARGET_PRESSURE_CMH2O;
  vvs.md_specifics.pcv.max_volume_ml = MAX_VOLUME_ML;
}

void test_enact_parameters() {
  // Here call get value many times, then set Values, then test
  // that the vs is actually correct!
  setValue8(VENTILATION_MODE_8,VCV_MODE);
  TEST_ASSERT_EQUAL(getValue8(VENTILATION_MODE_8),VCV_MODE);
  enact_parameters();
  TEST_ASSERT_EQUAL(VCV_MODE,vvs.mode.mode);
  setValue8(VENTILATION_MODE_8,PCV_MODE);
  enact_parameters();
  TEST_ASSERT_EQUAL(getValue8(VENTILATION_MODE_8),PCV_MODE);
}

void test_32_params() {
  // Here call get value many times, then set Values, then test
  // that the vs is actually correct!
  setValue32(PCV_TARG_PRESSURE_MMH2O_32,410);
  setValue32(PCV_MAX_VOL_ML_32,358);
  setValue32(VCV_TARG_VOL_ML_32,357);
  setValue32(VCV_MAX_PRESSURE_MMH2O_32,411);

  enact_parameters();
  TEST_ASSERT_EQUAL(getValue32(PCV_TARG_PRESSURE_MMH2O_32),410);
  TEST_ASSERT_EQUAL(getValue32(PCV_MAX_VOL_ML_32),358);
  TEST_ASSERT_EQUAL(getValue32(VCV_TARG_VOL_ML_32),357);
  TEST_ASSERT_EQUAL(getValue32(VCV_MAX_PRESSURE_MMH2O_32),411);
}
void test_16_params() {
  // Ain't got none!
}
void test_8_params() {
  // Here call get value many times, then set Values, then test
  // that the vs is actually correct!
  setValue8(VENTILATION_MODE_8,PCV_MODE);
  setValue8(RESPIRATION_RATE_BPM_8,11);
  setValue8(E_TO_I_TENTHS_8,90);

  enact_parameters();
  TEST_ASSERT_EQUAL(getValue8(VENTILATION_MODE_8),PCV_MODE);
  TEST_ASSERT_EQUAL(getValue8(RESPIRATION_RATE_BPM_8),11);
  TEST_ASSERT_EQUAL(getValue8(E_TO_I_TENTHS_8),90);
}

long last_time = 0;
long cur_time = 0;
void loop()
{
    UNITY_BEGIN();
    RUN_TEST(test_enact_parameters);
    RUN_TEST(test_32_params);
    RUN_TEST(test_16_params);
    RUN_TEST(test_8_params);
    UNITY_END();
    exit(0);
}

#ifdef ARDUINO
#else
int main(int argc, char **argv) {
  setup();
  while(1) loop();
}
#endif
