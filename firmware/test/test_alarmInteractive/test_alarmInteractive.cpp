#include <unity.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <alarm.h>
#include <timer.h>
#include <debug.h>


using namespace VentOS_Alarm;


void test_alarm_trigger() {
  AlarmModule a;
  // trigger ten second alarm as test 
  a.trigger(HIGH_PRESSURE, millis(), 5000);
  uint32_t start = millis();
  while(millis() < start+6000) {
    a.run(millis());
  }
}

void test_alarm_silence_interactive() {
  AlarmModule a;
  // trigger ten second alarm as test 
  while(true) {
    a.trigger(HIGH_PRESSURE, millis(), 500);
    a.run(millis());
  }

}

void process() {
  UNITY_BEGIN();
  // RUN_TEST(test_alarm_trigger);
  RUN_TEST(test_alarm_silence_interactive);
  //RUN_TEST(test_alarm_reset);
  UNITY_END();
}

#ifdef ARDUINO
#include <Arduino.h>
void setup() {
    // NOTE!!! Wait for > -->2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    delay(2000);
    process();
}
void loop() {
    //
}
#else
int main(int argc, char **argv) {
    process();
    return 0;
}
#endif
