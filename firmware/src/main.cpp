/*
To run this on any Arduino hardware, do something like:
  pio run -e featheresp32 -t upload
  pio device monitor
or:
  pio run -e uno -t upload
  pio device monitor

Single line run and montior:
  make pio-run-esp32
  make pio-run-uno

For native:
  make pio-run-native
*/

#ifdef ARDUINO
#include <Arduino.h>
#else // Native
#include <iostream>
#endif

//#include <controller.h>
#include <debug.h>
#include <sensor.h>
#include <state.h>
//#include <alarm.h>
#include <timer.h>
//#include <display.h>
//#include <input.h>
#include <machine.h>
#include <scheduler.h>

using namespace VentOS;
//using namespace VentOS_Alarm;

// Each of these classes inherits from Task
// so that they can run through the scheduler
//VentController vc;
//AlarmModule am;
//DisplayController dc;
//InputController ic;
// !!!!!! THESE TASKS HAVE BEEN MOVED TO BE DEPENDENCIES OF MACHINE CONTROLLER !!!!!

MachineController mc;

TaskScheduler ts;


void setup()
{
#ifdef ARDUINO
  Serial.begin(112500);
  delay(1000);
#endif

  DebugLn<const char*>("---------- VentOS Starting ----------");
  /*
  * Setup the (very simple) task scheduler.
  * It is interrupt controlled with a constant time slice.
  * Tasks must promise to complete within the timeslice or
  * there will a memory error - There is no context switching!
  * There are no dynamic RTOS features like pre-emption or
  * priority scheduling so that smaller controllers such as the
  * Uno/Nano can be used, and to ensure a highly derminisitic system.
  */

  // Setting mode to 0 uses software timers the same as MAIN_V1 below
  // Setting mode to 1 uses the fixed timeslice scheduler, and you must call ts.setTimeslice()
  ts.setSchedulerMode(0);

  //ts.addTask(0, &vc);
  ts.addTask(0, &mc);
  //ts.addTask(1, &amc);
  //ts.addTask(2, &dc);
  //ts.addTask(3, &ic);
  // !!!!!! THESE TASKS HAVE BEEN MOVED TO BE DEPENDENCIES OF MACHINE CONTROLLER !!!!!
  // Please set #define NUM_TASKS in tasks.h to match the number of tasks!

  ts.start();

  DebugLn("---------- End of Start Phase ----------");
}

void loop(){
  ts.loop();
  DebugLn("---------- Loop ----------");
}

#ifndef ARDUINO
int main(int argc, char **argv) {
  setup();
  while(1) loop();
  return 0;
}
#endif
