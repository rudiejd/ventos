#include "controlAPI.h"
#include <inttypes.h>
#include <debug.h>
#include <controller_task_PCV.h>

// Here are the basic holding pens for the values;
// I think it is wise to have a separate function move these
// values into the actual ventilator state.

// This could be done with an initialization function, but
// in a sense this is simpler.
extern struct volatile_ventilator_state vvs;

// Maybe this should be put in a class?
#define NUM_NAMES_8 3
#define NUM_NAMES_16 0
#define NUM_NAMES_32 4
uint8_t values_8[NUM_NAMES_8];
uint16_t values_16[NUM_NAMES_16];
uint32_t values_32[NUM_NAMES_32];

uint16_t name_to_index(uint16_t name,uint8_t size) {
  switch(size){
  case 8:
    switch(name) {
    case VENTILATION_MODE_8:
      return 0;
    case RESPIRATION_RATE_BPM_8:
      return 1;
    case E_TO_I_TENTHS_8:
      return 2;
    default:
      // If we reach here, this is a major internal error!
      DebugLn<const char *>("INTERNAL ERROR!");
      return UINT16_MAX;
      break;
    }
    break;
  case 16:
    switch(name) {
    default:
      // If we reach here, this is a major internal error!
      DebugLn<const char *>("INTERNAL ERROR!");
      return UINT16_MAX;
      break;
    }
    break;
  case 32:
    switch(name) {
    case MAX_PRESSURE_MMH2O_32:
      return 0;
    case PCV_TARG_PRESSURE_MMH2O_32:
      return 1;
    case PCV_MAX_VOL_ML_32:
      return 2;
    case VCV_TARG_VOL_ML_32:
      return 3;
    case VCV_MAX_PRESSURE_MMH2O_32:
      return 4;
    default:
      // If we reach here, this is a major internal error!
      DebugLn<const char *>("INTERNAL ERROR!");
      throw "INTERNAL ERROR: Bad Index!";
      break;
    }
    break;
  default:
      // If we reach here, this is a major internal error!
    DebugLn<const char *>("INTERNAL ERROR!");
    throw "INTERNAL ERROR: Bad Index!";
    break;
  }
  // This the best way to signal an error condition with an uint return type, I guess. --rlr
  DebugLn<const char *>("INTERNAL ERROR: Bad Index!");
  throw "INTERNAL ERROR: Bad Index!";
}

uint8_t getValue8(uint16_t name) {
  return values_8[name_to_index(name,8)];
}
uint8_t setValue8(uint16_t name,uint8_t value) {
   uint8_t prev = values_8[name_to_index(name,8)];
   values_8[name_to_index(name,8)] = value;
   return prev;
}

uint16_t getValue16(uint16_t name) {
  return values_16[name_to_index(name,16)];
}

uint16_t setValue16(uint16_t name,uint16_t value) {
   uint16_t prev = values_16[name_to_index(name,16)];
   values_16[name_to_index(name,16)] = value;
   return prev;
}

uint32_t getValue32(uint16_t name) {
  return values_32[name_to_index(name,32)];
}
uint32_t setValue32(uint16_t name,uint32_t value) {
   uint32_t prev = values_32[name_to_index(name,32)];
   values_32[name_to_index(name,32)] = value;
   return prev;
}


// return value true on success.
uint8_t enact_parameters() {
  // TODO: We could put a "sanity check" here before allowing the parameters
  // to be enacted...
  vvs.mode.mode = values_8[name_to_index(VENTILATION_MODE_8,8)];

  // This could fail if we can detect illegal values...
  return 1;
}

// uint32_t getValueBuff(uint16_t name,uint16_t len, char* value);
// uint32_t setValueBuff(uint16_t name,uint16_t len, char* value);
