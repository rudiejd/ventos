#ifndef CONTROL_API_H
#define CONTROL_API_H 1

#include <drive.h>
#include <inttypes.h>


// NOTE: This has not been squared with the "Data Dictionary" that Erich
// has created yet.

// These are parameter designators (or "names").
#define VENTILATION_MODE_8 1
#define RESPIRATION_RATE_BPM_8 2
#define E_TO_I_TENTHS_8 3
#define MAX_PRESSURE_MMH2O_32 0
#define PCV_TARG_PRESSURE_MMH2O_32 1
#define PCV_MAX_VOL_ML_32 2
#define VCV_TARG_VOL_ML_32 3
#define VCV_MAX_PRESSURE_MMH2O_32 4


// These are fixed values for those parameters that have fixed values.
#define PCV_MODE 0
#define VCV_MODE 1


uint8_t getValue8(uint16_t name);
uint8_t setValue8(uint16_t name,uint8_t value);

uint16_t getValue16(uint16_t name);
uint16_t setValue16(uint16_t name,uint16_t value);

uint32_t getValue32(uint16_t name);
uint32_t setValue32(uint16_t name,uint32_t value);

uint8_t enact_parameters();

// uint32_t getValueBuff(uint16_t name,uint16_t len, char* value);
// uint32_t setValueBuff(uint16_t name,uint16_t len, char* value);


#endif
