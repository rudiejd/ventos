#ifndef ARDUINO
#include <chrono>
//#include <iostream>
#endif
#include <timer.h>

#include <debug.h>

namespace VentOS {
#ifndef ARDUINO
    // The Linux time since 1970 - equivalant to millis() 
    // when used to find delta t for software timers
    uint64_t timeSinceEpochMs(){
        return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
    }
#endif

    bool Timer::update(){
#ifdef ARDUINO
        this->msElapsed = millis() - this->msStart;
#else
        this->msElapsed = (uint32_t)(timeSinceEpochMs()) - this->msStart;
#endif
        // TODO: error checks
        return true;
    }

	Timer::Timer(){
		msElapsed = 0;
		#ifdef ARDUINO
			msStart = millis();
		#else
			msStart = (uint32_t)(timeSinceEpochMs());
		#endif
		msLast = 0; 
	}

    uint32_t Timer::elapsed(){
        return msElapsed;
    }

    uint32_t Timer::getLast(){
        return msLast;
    }

    uint32_t Timer::diff() {
        return msElapsed - msLast;
    }

    uint32_t Timer::setLast(uint32_t ms){
        return msLast = ms;
    }
}
