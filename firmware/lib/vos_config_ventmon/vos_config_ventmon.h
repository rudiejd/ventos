/*
This file is HIGHLY VentMon specific; it is meant to be an initial example
of how we can use a config file to support various sensors, which
will be different for every team, with an abstract layer that
returns PIRDS data format.
 */

#ifndef VOS_CONFIG_VENTMON_H
#define VOS_CONFIG_VENTMON_H
#include <sensor.h>

// This will likely be redefinition, but that is okay...
#define SENSOR_CNT 1

extern sensor SENSORS[];

#endif
