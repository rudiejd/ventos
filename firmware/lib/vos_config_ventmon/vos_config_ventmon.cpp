#include "vos_config_ventmon.h"
#include <sensor.h>
#include <sensor_honeywell.h>

uint8_t sensor_cnt = SENSOR_CNT;

sensor SENSORS[SENSOR_CNT] = { { .type = 'D',
                        .loc = 'I',
                        .num = 0,
                        .func = &differential_sensor }};
