#include <stdio.h>
#include "sensor.h"

// A Utility...
// return null on failure...
sensor_func find_sensor(sensor ss[], uint8_t type, uint8_t loc, uint8_t num) {
  for(int i = 0; i <= sensor_cnt; i++) {
    if ((ss[i].type == type) &&
        (ss[i].loc == loc) &&
        (ss[i].num == 0)) {
      return ss[i].func;
    }
  }
  return 0;
}
