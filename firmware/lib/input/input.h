#ifndef INPUT_H
#define INPUT_H

#include <PIRDS.h>
//#include <PIRCS.h>
#include <task.h>

namespace VentOS {
    
    // The input implementation should inherit from this class
    class InputController : public Task {
        public:
            InputController(){}
            bool setup();
            bool run(uint32_t msNow);
            virtual bool connect();
            virtual bool getMessage(Message m);
            virtual bool getMeasurement(Measurement m);
            // ...
    };

}

#endif