#include <input.h>
#include <PIRDS.h> // Should be PIRCS
#include <task.h>
#include <debug.h>

namespace VentOS {

    bool InputController::setup(){
        DebugLn<const char*>("Input controller setup");
        return true;
    }

    bool InputController::run(uint32_t msNow){
        DebugLn<const char*>("Input controller run");
        return true;
    }

    bool InputController::connect(){
        
        return true;
    }
    
    bool InputController::getMessage(Message m){
        
        return true;
    }

    // Get PIRCS measurement from display.
    bool InputController::getMeasurement(Measurement m){
        
        return true;
    }


}