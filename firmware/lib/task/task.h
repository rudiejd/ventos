#ifndef TASK_H
#define TASK_H

#include <inttypes.h>

namespace VentOS {

class Task {
    public:
        Task(){}
        virtual bool setup();
        virtual bool run(uint32_t msNow);
};

}

#endif