#include <vos_config_mock.h>
#include <sensor.h>
#include "vos_sim_config.h"
#include <controller.h>

using namespace VentOS;

//unsigned long simulatedPressureReading(bool);
// TODO: Any mock sensors must be a function of the ventilator state.
// We probably therefore need an initialization function which establishes
// a hidden local for us to look at.

uint8_t sensor_cnt = SENSOR_CNT;

// VentState
struct volatile_ventilator_state *vs_for_mocking;

void set_ventilator_state(struct VentState *v) {
  vs_for_mocking = v->vvs;
}

// Note this is a "perfect" sensor and assumes perfect
// air production; we should probably add noise and other
// imperfections to it.
// Note: this returns a PIRDS pressure (10ths of cmH2O)
uint32_t diff_pressure_sensor() {

#if 1	// could use a compiler flag, say VOS_USE_SIMULATED_PRESSURE_SENSOR
	// need to discuss/decide aappropriately
  if (vs_for_mocking) {
    if (!vs_for_mocking->client_data)
      vs_for_mocking->client_data= getDefaultInternalTestConfiguration();
    return simulatedMockPressureReading(vs_for_mocking);
  }
  else
// #else
  return (vs_for_mocking->inspiring ?
          vs_for_mocking->md_specifics.pcv.target_pressure_cmH2O*10 :
          0);
#endif
}


// Modify/rename/remove this method appropriately based on design
unsigned long use_abnormal_pressure_reading() {
  if (vs_for_mocking) {
    if (!vs_for_mocking->client_data)
      vs_for_mocking->client_data= getDefaultInternalTestConfiguration();
    return simulatedMockAbnormalPressureReading(vs_for_mocking);
  }

  // do what is appropriate, if mock-config is not available.
  return (vs_for_mocking->inspiring ?
          vs_for_mocking->md_specifics.pcv.target_pressure_cmH2O*10 :
          0);
}


sensor SENSORS[SENSOR_CNT] = { { .type = 'D',
                        .loc = 'I',
                        .num = 0,
                        .func = &diff_pressure_sensor }};
