#ifndef VOS_CONFIG_MOCK_H
#define VOS_CONFIG_MOCK_H

#include <sensor.h>
#include <vos_simulator.h>
#include <controller.h>

// using namespace VOS_Controller;

// This will likely be redefinition, but that is okay...
#define SENSOR_CNT 1

extern sensor SENSORS[];

unsigned long simulatedPressureReading(bool);

void set_ventilator_state(VentState *v);

#endif
