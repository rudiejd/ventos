#include <machine.h>
#include <debug.h>
#include <task.h>
#include <controller.h>
#include <alarm.h>
#include <display.h>
#include <input.h>
//#include <machine_config.h>

#ifdef ARDUINO
#include <Arduino.h>
#else // Native
#include <iostream>
#endif

using namespace VentOS_Alarm;

namespace VentOS {

    VentController vc;
    AlarmModule am;
    DisplayController dc;
    InputController ic;

    ////////// METHODS //////////

    bool configSensors(SensorConfig *sensors){
        for (int i = 0; i < SENSOR_LENGTH; i++){
            switch (sensors[i].comm){
                case DIGITAL_SENSOR:
                    DebugLn("Digital sensor");
                break;
                case ADC_SENSOR:
                    DebugLn("ADC sensor");
                break;
                case I2C_SENSOR:
                    DebugLn("I2C sensor");
                    DebugLn<uint8_t>(sensors[i].addr);
                    // Setup driver
                break;
                case SERIAL_SENSOR:
                    DebugLn("Serial sensor");
                break;
                default:
                    DebugLn("ERROR: Sensor comm not supported!");
                    return false;
                break;
            }
        }

        return true;
    }

    bool MachineController::setup(){
        DebugLn<const char*>("Machine controller setup");
        // TODO: Add true/false checks on startup
        if (!vc.setup()){
            DebugLn("ERROR: Controller setup critical failure!");
            return false;
        }
        if (!am.setup()){
            DebugLn("ERROR: Alarm setup critical failure!");
            return false;
        }
        if (!dc.setup()){
            DebugLn("ERROR: Display setup critical failure!");
            return false;
        }
        if (!ic.setup()){
            DebugLn("ERROR: Input setup critical failure!");
            return false;
        }
        return true;
    }
    
    bool MachineController::run(uint32_t msNow){
        DebugLn("Machine controller run");

        switch (stage){
            case Standby:
                if (bootMachine(myConfig) == MachineSuccess){
                    stage = Starting;
                }
                else {
                    stage = Standby;
                }
            break;
            case Starting:
                if (startMachine() == MachineSuccess){
                    stage = Ready;
                } else {
                    stage = Starting;
                }
            break;
            case Ready:
                stage = Setup;
            break;
            case Setup:
                // Setup the ventilator here
                if (setupMachine(true) == MachineSuccess){
                    stage = MachineRunning;
                } else {
                    stage = Setup;
                }
            break;
            case MachineRunning:            
                {
                    // Run ventilation mode and trigger alarm if needed
                    AlarmEvent ae = vc.runVentilation(msNow);
                    if (ae != None){
                        am.trigger(ae, msNow, 60);
                        return false;
                    }

                    if (!am.run(msNow)){
                        DebugLn("ERROR: Alarm critical failure!");
                        return false;
                    }

                    if (!dc.run(msNow)){
                        DebugLn("ERROR: Display critical failure!");
                        return false;
                    }
                    
                    if (!ic.run(msNow)){
                        DebugLn("ERROR: Input critical failure!");
                        return false;
                    }
                }
            break;
            case Disabled:
                DebugLn("Machine disabled!");
            break;
            case ShuttingDown:
                DebugLn("Machine shutting down!");
            break;
            default:
                DebugLn("ERROR: Machine critical failure! Invalid status!");
                return false;
            break;
        }

        return true;
    } 

    // Very first function that is called when the ventilator is powered on
    MachineResult MachineController::bootMachine(MachineConfig config){
        DebugLn<const char*>("bootMachine");
        // Read config file and set pins etc. 

        // Validate machine config
        if (validateConfig(config)){
            DebugLn("Machine config validated!");
        } else {
            DebugLn("Machine config INVALID!");
            return MachineConfigError;
        }

        if (configSensors(config.sensors)){
            DebugLn("Sensors configured!");
        } else {
            DebugLn("Sensors not configured!");
            return MachineSensorConfigError;
        }
        
        // Run startup diagnostics
        // .....
        

        return MachineSuccess;
    }

    
    MachineResult MachineController::setupMachine(bool autoSetup){
        DebugLn<const char*>("setupMachine");
        // Wait for settings to be inputted
        if (autoSetup){
            Debug("Auto setup mode");
            // TODO: set defaults
        } else {
            Debug("Manual setup mode");

            // This is just a console demo for testing (to be replaced)
            #ifndef ARDUINO
            
                uint16_t in[5];

                std::cout << "Ventilator mode (0 = PCV, 1 = VCV): ";
                std::cin >> in[0];
                VentMode m = static_cast<VentMode>(in[0]);

                std::cout << "Respiration rate (1-10): ";
                std::cin >> in[1];

                if (m == PCV){
                    std::cout << "Target pressure (10-50cmH20): ";
                    std::cin >> in[2];
                } else {
                    std::cout << "Target volume (200-800ml): ";
                    std::cin >> in[2];
                }

                std::cout << "EI ratio (1-4): ";
                std::cin >> in[3];

                std::cout << "PEEP (0-15cmH20): ";
                std::cin >> in[4];

                if (m == PCV){
                    vc.setupPCVMode(in[1],in[2],in[3],in[4], SQUARE);
                } else if (m == VCV) {
                    vc.setupVCVMode(in[1],in[2],in[3],in[4], SQUARE);
                } else {
                    Debug("Mode selection is invalid!");
                    return MachineFailed;
                }

            #else
            // Uno/ESP32 serial input here...
            // TODO: Ling

            #endif
        }

        return MachineSuccess;
    }


    MachineResult MachineController::startMachine(){
        DebugLn<const char*>("startMachine");
        

        return MachineSuccess;
    }

    bool validateConfig(MachineConfig config) {
        // TODO: make this better
        if (config.format_version > 0){
            return true ;
        }

        return false;
    }

}