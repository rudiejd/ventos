#ifndef DRIVER_LIST
#define DRIVER_LIST

namespace VentOS{
// 0-99 are sensors
// 0-29 are pressure sensors
#define HONEYWELL_HSCDRRT001PD2A3 0

// 30-59 are flow sensors
#define SENSIRION_SMX3000 30

// 60-79 are gas sensors
#define TELEDYNE_O2 60
#define TELEDYNE_CO2 61

// 80-99 are other sensors
#define TEMPERATURE 80
#define HUMIDITY 81

// 100-139 are drives (solenoids, motors)
#define PROP_SOLENOID_PWM 100
#define SOLENOID_DIGITAL 101
#define BLOWER_PWM 110
#define STEPPER_DRV8806 120

// 140-169 are displays
#define LCD_TOUCH_3INCH 140
#define LCD_TOUCH_4INCH 141
#define LCD_TOUCH_10INCH 142
#define LCD_TOUCH_17INCH 143
#define OLED_BADGE_1INCH 150
#define EPAPER_4INCH 151
#define PHONE_4INCH 161
#define PHONE_6INCH 162
#define TABLET_10INCH 162

// 170-200 are networking
#define ETHERNET_DRIVER 170
#define WIFI_DRIVER 171
#define BLUETOOTH_DRIVER 172
#define LORA_DRIVER 173
#define SERIAL_DRIVER 180
#define USB_DRIVER 181

// 200-219 are digital inputs
#define GENERIC_INPUT 200
#define PUSH_BUTTON 201
#define SWITCH 202
#define ROTARY_ENCODER 203
#define ADC 204
#define TOUCH_XMIN 210
#define TOUCH_XMAX 211
#define TOUCH_YMIN 212
#define TOUCH_YMAX 213

// 220-239 are digital outputs
#define GENERIC_OUTPUT 220
#define LED 221
#define PIEZO 222
#define DAC 223

// 240-256 are reserved



#define DIGITAL_SENSOR 0
#define ADC_SENSOR 1
#define I2C_SENSOR 2
#define SERIAL_SENSOR 3
}
#endif