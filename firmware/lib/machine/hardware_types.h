#ifndef HARDWARE_TYPES_H
#define HARDWARE_TYPES_H

#include <inttypes.h>
namespace VentOS {
    struct NetworkingConfig {
        uint8_t type;
        bool isEnabled;
        uint32_t address;
        uint8_t driver;
    };

    struct SensorConfig {
        uint8_t type;
        uint8_t location;
        uint8_t number;
        uint8_t pin;
        uint8_t comm;  // digital, ADC, I2C, Serial
        uint8_t addr;  // I2C address
        uint8_t driver;
    };

    struct DriveConfig {
        uint8_t type;
        uint8_t location;
        uint8_t number;
        uint8_t driver;
    };

    struct DisplayConfig {
        uint8_t type;
        uint16_t width;
        uint16_t height;
        uint8_t refresh;
        uint8_t touch; // 0 = none, 1 = resistive, 2 = capactive
        uint8_t driver;
    };

    struct IOConfig {
        uint8_t type;
        uint8_t feature;
        uint8_t pin;
        uint8_t driver;
    };
}
#endif