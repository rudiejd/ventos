#ifndef MACHINE_H
#define MACHINE_H

#include <inttypes.h>
#include <task.h>
#include <driver_list.h>
#include <machine_config.h>
#include <hardware_types.h>

namespace VentOS {
    bool validateConfig(MachineConfig config);
    
    enum PartStatus {
        GOOD, // Within parameters
        WARNING, // Part needs servicing soon
        ERROR_ACCURACY, // Errors when sensor parameters are out of bounds
        ERROR_PRECISION,
        ERROR_DISCONNECTED,
        ERROR_FAILURE
    };

    enum MachineResult {
        MachineFailed,
        MachineSuccess,
        MachineError1,
        MachineConfigError,
        MachineSensorConfigError
        // ....
    };

    enum MachineStage {
        Standby, // Powered on, but hasn't received commands in x (5?) minutes
        Starting, // Uninteruptable startup phase
        FactoryTest, // Long running burn-in test at the factory
        StartupTest, // Automatic test run during every startup
        ServiceTest, // Technician triggered diagnostic test
        SelfTest, // Very short automatic test that runs periodically while the ventilator is on
        Ready, // Ready to do something
        Setup, // Setup the ventilator
        MachineRunning, // Ventilator is ventilating
        //Busy, // Busy doing something else besides a ventilation task
        //PausedM, // Manually paused until manually unpaused
        //Paused3Sec, // Manually paused for 3 seconds (insp hold manouver)
        //Alarm, // Ventilator has stopped because an alarm has been raised
        Disabled, // eg. emergency stop
        ShuttingDown, // Uninterruptable shutdown sequence
        Restarting // Uninterruptable restart sequence
    };

    struct MachineState {
        PartStatus batteryStatus = GOOD;
        PartStatus mainsPowerStatus = GOOD;

    };
    
    class MachineController : public Task {
        private:
        MachineStage stage;
        MachineState state;

        public:
        MachineController(){
            //
        }

        bool setup();
        bool run(uint32_t msNow);
        MachineResult bootMachine(MachineConfig config); // Very first method to call
        MachineResult setupMachine(bool autoSetup);
        MachineResult startMachine();
        MachineResult restartMachine();
        MachineResult shutdownMachine();

    };

}

#endif
/*

      // Localization could be in another class?
      bool setLanguage();
      bool setTimezone();
      bool setPatientUnits();

      // Diagnostic testings could be in another class?
      bool runSelfTest();
      bool runStartupTest();
      bool runFactoryTest();
      bool runServiceTest();

      // History could be in another class?
      bool addHistory();
      bool readHistory(int i);
      bool getHistoryBetween(int i, int j);
      bool getAllHistory();
      bool clearHistory();
*/




     /*struct MachineConfig {
        const char* format_version;
        const char* vent_model;
        const char* vent_manufacturer;
        const char* vent_supplier;
        const char* vent_version;
        NetworkingConfig networking[NETWORKING_LENGTH];
        SensorConfig sensors[SENSOR_LENGTH];
        DriveConfig drives[DRIVE_LENGTH];
        DisplayConfig display;
        IOConfig inputs[INPUT_LENGTH];
        IOConfig outputs[OUTPUT_LENGTH];
    };  */