#ifndef CONTROLLER_TASK_PCV_H
#define CONTROLLER_TASK_PCV_H 1

#include <drive.h>
#include <inttypes.h>
using namespace VentOS;

// NOTE: This is an example of where the "Data Dictionary" would be useful to us
// Quite possibly that should simply be a .h file (at least, one embodiment of it is.)
// TODO: The Ventilator state here should probably be moved out to its own file.
#define PCV_MODE 0
#define VCV_MODE 1

struct universal_mode {
  uint8_t mode;
  float rr_bpm;
  float e_to_i;
  float max_pressure_cmH2O;
};

struct pcv_mode {
  float target_pressure_cmH2O;
  float max_volume_ml;
};

struct vcv_mode {
  float target_volume_ml;
  float max_pressure_cmH2O;
};

union ModeSpecifics {
    pcv_mode pcv;
    vcv_mode vcv;
};

struct volatile_ventilator_state  {
  VentOS::Drive *d;
  uint8_t inspiring; // 1 = true, inspiration, 0 = false, exhalation
  uint32_t ms_into_breath;
  universal_mode mode;
  ModeSpecifics md_specifics;
  void *client_data; // supply simulator config info through this, be extra careful how it is set; wrong type of data would cause system fail.
};


typedef volatile_ventilator_state* (*task_run_ms)(volatile_ventilator_state *v,uint32_t t);

struct task {
    task_run_ms run;
};

// Run experimental Pressure Controlled Ventilation for t millisedons
// Take a state and return a state
volatile_ventilator_state* run_experimental_pcv(volatile_ventilator_state *v,unsigned t);
volatile_ventilator_state* run_experimental_vcv(volatile_ventilator_state *v,unsigned t);

#define TASK_CNT 3

#define PCV_TASK 0
#define VCV_TASK 1
#define NO_OP_FOR_TESTING_TASK 2

extern task CONTROLLER_TASKS[TASK_CNT];

void controller_task_init();

#endif
