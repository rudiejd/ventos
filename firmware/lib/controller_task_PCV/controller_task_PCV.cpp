#include "controller_task_PCV.h"
#include <drive.h>
#include <inttypes.h>
#include <debug.h>


uint32_t ms_in_inspiration(const universal_mode *mode) {
  float breath_length = 60.0 / mode->rr_bpm;
  return (uint32_t) 1000.0 * breath_length * (1.0/(mode->e_to_i) + 1.0);
}

volatile_ventilator_state* run_experimental_pcv(volatile_ventilator_state *v,uint32_t t) {
  // simply toggle...
  // A major goal now is to to have this use the drive.
  // In presure controlled ventilation, we have a "MAXIMUM_VOL", not a target VOL.

  const uint32_t insp_dur_ms = ms_in_inspiration(&(v->mode));
  DebugLn<const char *>("AAA");

  const uint32_t MAX_PRESSURE = v->mode.max_pressure_cmH2O;
  DebugLn<const char *>("BBB");

  const uint32_t MAXIMUM_VOL = v->md_specifics.pcv.max_volume_ml;
  DebugLn<const char *>("CCC");
  const uint32_t TARGET_PRESSURE = v->md_specifics.pcv.target_pressure_cmH2O;
  //  const uint32_t DRIVE_INTERVAL_MS = 10;
  DebugLn<const char *>("DDD");
  const float MAXIMUM_FLOW_MLPS = (float) MAXIMUM_VOL /  ((float) insp_dur_ms / 1000.0);

  DebugLn<const char *>("EEE");
  if (v->ms_into_breath < insp_dur_ms) {
      // We are still in inspriation, so call drive_pressure
      DebugLn<const char *>("FFF");
      uint32_t d_res = v->d->drive_pressure((uint32_t) MAXIMUM_FLOW_MLPS,TARGET_PRESSURE,
                                          MAX_PRESSURE,
                                          (char *) v->client_data,"spud");
      Debug<uint32_t>(d_res);
      DebugLn<const char *>(" : GGG");
  }

  v->inspiring = (v->ms_into_breath < insp_dur_ms);
  return v;
}

volatile_ventilator_state* no_op_for_testing_pcv(volatile_ventilator_state *v,uint32_t t) {
  return v;
}

volatile_ventilator_state* run_experimental_vcv(volatile_ventilator_state *v,uint32_t t) {
  // simply toggle...
  // A major goal now is to to have this use the drive.

  const uint32_t insp_dur_ms = ms_in_inspiration(&(v->mode));

  const uint32_t MAX_PRESSURE = v->mode.max_pressure_cmH2O;

  const uint32_t TARGET_PRESSURE = v->md_specifics.pcv.target_pressure_cmH2O;

  const uint32_t TARGET_VOL = v->md_specifics.vcv.target_volume_ml;
  //  const uint32_t DRIVE_INTERVAL_MS = 10;
  const float TARGET_FLOW_MLPS = (float) TARGET_VOL /  ((float) insp_dur_ms / 1000.0);
  //  const uint32_t INTERVALS = insp_dur_ms / DRIVE_INTERVAL_MS;

  if (v->ms_into_breath < insp_dur_ms) {
    uint32_t d_res = v->d->drive_flow((uint32_t) TARGET_FLOW_MLPS,TARGET_PRESSURE,
                                      MAX_PRESSURE,(char *) v->client_data,"spud");
    DebugLn<uint32_t>(d_res);
  }

  Debug<const uint32_t>(((long *)v->client_data)[0]);

  v->inspiring = (v->ms_into_breath < insp_dur_ms);
  return v;
}

task CONTROLLER_TASKS[TASK_CNT];

void controller_task_init() {
  task pcv = { .run = &run_experimental_pcv };
  CONTROLLER_TASKS[PCV_TASK] = pcv;
  task vcv = { .run = &run_experimental_vcv };
  CONTROLLER_TASKS[VCV_TASK] =  vcv;
  task noop = { .run = &no_op_for_testing_pcv };
  CONTROLLER_TASKS[NO_OP_FOR_TESTING_TASK] =  noop;
}
