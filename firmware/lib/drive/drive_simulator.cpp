#include "drive.h"
#include <drive_simulator.h>

// #include <debug.h>

namespace VentOS{

 // return number is expected flow in milliliters per second that the air drive thinks it can return
 // flow_mlps is the require flow in milliliters pers second
 // at_pressure_cmH2O_tenths is the pressure in the mm of water against which the air drive must operate
 // duration_ms is the time that the this flow should be produced in ms
 // from_now_ms is when to begin producing this flow. This can be used to allow the drive to prepare for action.
 // client_data is a pointer to a 64 byte buffer guaranteeed to be returned on the next call undisturbed.
 // custom is a pointer to an character buffer of unspecified size. It is used only for data specific to the given airdrive.
uint32_t Drive::drive_flow (uint32_t flow_mlps,
                            uint32_t at_pressure_cmH2O_tenths,
                            uint32_t max_pressure_cmH2O_tenths,
                            char *client_data,
                            const char *custom) {
  // Debug<const char*>("driver driving - flow: ");
  // Debug<uint32_t>(flow_mlps);
  // Debug<const char*>(", pressure: ");
  // Debug<uint32_t>(at_pressure_cmH2O_tenths);

    // Send the data over SPI
    // ...
    // ...

    // This is the native version...in reality you would
    // have to really interface with the hardware
  // Debug<const char*>("drive called: ");
  // Debug<uint32_t>(flow_mlps);
  // Debug<uint32_t>(at_pressure_cmH2O_tenths);
  // Debug<uint32_t>(duration_ms);
  // Debug<uint32_t>(from_now_ms);
  // Debug<const char*>(custom);
  // Just to be sneaky, I will add the volume produced since the last pause_and_prep call to my client_data!
  // This is actually in microliters (millionths of a liter)
  // I was using the duration here, but I have removed it.
  // I will have to give the simulator access to a global clock to compute this.


  // TODO: Read this from the last call based on a global clock (only for testing.)
  // This has not been implemented yet.
  const uint32_t duration_ms = 10;
  int v_millionths_l = flow_mlps*duration_ms;
  ((uint32_t *)client_data)[0] += v_millionths_l;

  // Being a simulation, this is a "perfect" drive...
  return flow_mlps;
}

uint32_t Drive::drive_pressure (uint32_t flow_mlps,
                                uint32_t at_pressure_cmH2O_tenths,
                                uint32_t max_pressure_cmH2O_tenths,
                                char *client_data,
                                const char *custom) {
  // Debug<const char*>("driver driving - flow: ");
  // Debug<uint32_t>(flow_mlps);
  // Debug<const char*>(", pressure: ");
  // Debug<uint32_t>(at_pressure_cmH2O_tenths);

    // Send the data over SPI
    // ...
    // ...

    // This is the native version...in reality you would
    // have to really interface with the hardware
  // Debug<const char*>("drive called: ");
  // Debug<uint32_t>(flow_mlps);
  // Debug<uint32_t>(at_pressure_cmH2O_tenths);
  // Debug<uint32_t>(duration_ms);
  // Debug<uint32_t>(from_now_ms);
  // Debug<const char*>(custom);
  // Just to be sneaky, I will add the volume produced since the last pause_and_prep call to my client_data!
  // This is actually in microliters (millionths of a liter)

  // We will return here the pressure that we think we can deliver;
  // we will assume this is a perfect device for now!
  ((uint32_t *)client_data)[0] = at_pressure_cmH2O_tenths;

  // Being a simulation, this is a "perfect" drive...
  return at_pressure_cmH2O_tenths;
}


uint32_t Drive::pause_and_prep(uint32_t timer_ms, char* client_data, const char *custom){
  //    Debug<const char*>("driver pausing and preping\n");
  //  Debug<const char*>("pause_and_prep called: ");
  //  Debug<uint32_t>(timer_ms);
  // Just to be sneaky, I will add the volume produced since the last pause_and_prep call to my client_data!
  ((long *)client_data)[0] = 0;
  return timer_ms;
}

int version = DRIVE_SIMULATOR_VERSION;
}
