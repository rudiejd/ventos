#ifndef DRIVE_H
#define DRIVE_H

#include <inttypes.h>

namespace VentOS{

  class Drive {
  public:
    uint32_t drive (uint32_t flow_mlps, uint32_t at_pressure_cmH2O_tenths, uint32_t duration_ms, uint32_t from_now_ms, char *client_data, const char *custom);
    uint32_t pause_and_prep(uint32_t duration_ms, char* client_data, const char *custom);
};

}

#endif
