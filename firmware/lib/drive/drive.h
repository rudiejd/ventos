#ifndef DRIVE_H
#define DRIVE_H

#include <inttypes.h>

// These are just magic numbers to prove we are reading
// the correct version.
#define DRIVE_SIMULATOR_VERSION 357
#define DRIVE_SIMULATOR_UNO_VERSION 222

namespace VentOS{

  class Drive {
  public:
    uint32_t drive_flow (uint32_t flow_mlps,
                         uint32_t at_pressure_cmH2O_tenths,
                         uint32_t max_pressure_cmH2O_tenths,
                         char *client_data,
                         const char *custom);
    uint32_t drive_pressure (uint32_t flow_mlps,
                             uint32_t at_pressure_cmH2O_tenths,
                             uint32_t max_pressure_cmH2O_tenths,
                             char *client_data,
                             const char *custom);

    uint32_t pause_and_prep(uint32_t duration_ms, char* client_data, const char *custom);
};

  extern int version;
}

#endif
