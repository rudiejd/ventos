#include <controller.h>
#include <debug.h>
#include <task.h>
#include <drive_simulator/drive_simulator.h>
#include <localization.h>
#include <alarm.h>

using namespace VentOS_Alarm;

namespace VentOS {

  ////////// FUNCTIONS //////////
  
  void CalculateWaveform(WaveformShape waveform){
    
    
  }

  ////////// METHODS //////////

  /*Drive dr;
  bool r = false;
  char* cd;
  char *custom;

  uint32_t tLast = 0;
  
  bool inspiring = true;
  uint32_t msStart = 0;
  
  uint32_t tPeriod = 2000; //ms
  uint32_t flowInsp = 10;
  uint32_t flowExh = 0;
  uint32_t pressureInsp = 400;
  uint32_t pressureExh = 50;
*/

    bool VentController::setup(){
        DebugLn<const char*>("Vent controller setup");

        return true;
    }
    
    bool VentController::run(uint32_t msNow){
        DebugLn("Vent controller run");
        //runVentilation(msNow);
        return true;
    }


    AlarmEvent VentController::runVentilation(uint32_t msNow){
        DebugLn<const char*>("runVentilation");

        // Run algorithms here
        switch (settings.mode){
            case PCV:
                if (!runPCVMode(msNow)){
                    DebugLn("ERROR: Critical error running PCV ventilation!");
                    return VentCriticalPCV;
                }
            break;
            case VCV:
                if (!runVCVMode(msNow)){
                    DebugLn("ERROR: Critical error running VCV ventilation!");
                    return VentCriticalPCV;
                }
            break;
            case PSV:
            
            break;
            case PRVC:

            break;
            default:
                    DebugLn<const char*>("Ventilation mode invalid!");
                    return VentInvalid;
            break;
        }

        return None;
    }



    VentState VentController::getState() {
        return this->state;
    }

    VentState VentController::setState(VentState state) {
        return this->state = state;
    }

    bool VentController::setupPCVMode(uint16_t rr, uint16_t pi, uint16_t eiRatio, uint16_t peep, WaveformShape waveform) {
        
        settings.mode = PCV;
        settings.targetRR = rr;
        settings.targetPressure = pi;
        settings.targetEI = eiRatio;
        settings.targetPEEP = peep;
        settings.waveform = waveform;
        
        // TODO: Calculate waveform values
        //                                       
        //       |``````|          |``..
        //       |      |          |    ``| 
        //  _____|      |    ______|      |

        return true;
    }

    
    bool VentController::setupVCVMode(uint16_t rr, uint16_t vi, uint16_t eiRatio, uint16_t peep, WaveformShape waveform) {
        
        settings.mode = VCV;
        settings.targetRR = rr;
        settings.targetVolume = vi;
        settings.targetEI = eiRatio;
        settings.targetPEEP = peep;
        settings.waveform = waveform;
        
        // TODO: Calculate waveform values
        //                                       
        //       |``````|          |``..
        //       |      |          |    ``| 
        //  _____|      |    ______|      |

        return true;
    }

    bool VentController::runPCVMode(uint32_t msNow){
        DebugLn("Running PCV Ventilation");

        // This is where the algorithm should go

        return true;
    }

    bool VentController::runVCVMode(uint32_t msNow){
        DebugLn("Running VCV Ventilation");

        // This is where the algorithm should go

        return true;
    }

}













  // This has been replaced by run()
  //VentState VentController::update(VentState vs, uint32_t *msNow) {
  /*bool VentController::update(uint32_t msNow){
    // Update the state
    // Must only run for ms
    //uint32_t timer = 0;
    //do {
      //vs.inspPressure++;
      //Debug<const char*>(".");
      //timer++; // replace with timer += time for this iteration
    //} while (timer < ms);

    //return vs;
    
    return true;
  }*/


  /*int ms_in_inspiration(const pcv_mode *pcv) {
    float breath_length = 60.0 / pcv->rr_bpm;
    return (int) 1000.0 * breath_length * (1.0/(pcv->e_to_i) + 1.0);
  }*/

  /*VentState* run_pcv(VentState *v,unsigned t) {
    // Toggle between inspiration and exhalation
    v->inspiring = (v->ms_into_breath < ms_in_inspiration(&(v->mode)));

    return v;
  }*/

  // This will likely be redefinition, but that is okay...
  /*task TASKS[TASK_CNT] =  {{ .run = &run_pcv }};

  void controller_task_init() {

    // TASKS[0] =  { .run = &run_experimental_pcv };

  }*/



    // Run algorithm
    // ...
    // ...


    // ERROR: Segmentation fault (core dumped) - occurs after 2000ms
    // This is a really bad test algorithm for PolyVent driver
    // Pause and prep is the reseting of the bellows that
    // occurs after every compression phase
/*
    if (msNow > (tLast + tPeriod)){
      inspiring = !inspiring;
      tLast = msNow;
    }

    if (inspiring){
      dr.drive(flowInsp, pressureInsp, 5, msNow, cd, custom);
    } else {
      //dr.drive(flowExh, pressureExh, 5, msNow, cd, custom);
      dr.pause_and_prep(msNow, cd, custom);
    }
    */