#include <alarm_hardware.h>
#include <debug.h>

#ifdef ARDUINO
#include <Arduino.h>
#endif

namespace VentOS_Alarm {

    void AlarmHardware::setPins(int led){
        #ifdef ARDUINO
            pinMode(led, OUTPUT);
        #endif
    }

    void AlarmHardware::setInPins(int pin){
        #ifdef ARDUINO
            pinMode(pin, INPUT);
        #endif
    }

    void AlarmHardware::triggerAlarm(AlarmEvent event, AlarmState state){
        VentOS::Debug("triggerAlarm: ");
        VentOS::DebugLn(state);
        #ifdef ARDUINO
            digitalWrite(event, HIGH);
        #else
            DebugLn("Writing high to pin: ")
            DebugLn(event);
        #endif
    }

    void AlarmHardware::stopAlarm(AlarmEvent event){
        #ifdef ARDUINO
            digitalWrite(event, LOW);
        #else
            DebugLn("Writing low to pin: ");
            DebugLn(event);
        #endif
    }

    uint8_t AlarmHardware::readInput(AlarmEvent event){
        #ifdef ARDUINO
			return digitalRead(event+(uint8_t)NUM_EVENTS);
        #else
            DebugLn("Reading pin: ");
            DebugLn(event + (uint8_t) NUM_EVENTS);
        #endif
    }

	AlarmHardware::AlarmHardware() {
		for (uint8_t i = 0; i < (uint8_t) NUM_EVENTS; i++) {
			setPins(i);
			setInPins((uint8_t)NUM_EVENTS + i);
		}	
	}


}
