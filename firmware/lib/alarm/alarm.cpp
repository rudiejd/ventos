#include "alarm_data.h"
#include <alarm.h>
#include <debug.h>
#include <task.h>
#include <timer.h>
#include <alarm_hardware.h>
using namespace VentOS;

namespace VentOS_Alarm {
  
  bool AlarmController::addRecord(AlarmRecord ar){
    if (arIndex >= 0 && arIndex < arLength){
      alarmRecords[arIndex] = ar;
      arIndex++;
      return true;
    } else if (arIndex >= arLength){
      arIndex = 0;
      alarmRecords[arIndex] = ar;
      return true;
    } else {
      return false;
    }
  }

  AlarmRecord AlarmController::getRecord(int i){
    //  Check limits
    return this->alarmRecords[i];
  }

  AlarmState AlarmController::setState(AlarmState state){
    return this->state = state;
  }

  AlarmState AlarmController::getState(){
    return this->state;
  }

  AlarmEvent AlarmController::setEvent(AlarmEvent event){
    return this->event = event;
  }

  AlarmEvent AlarmController::getEvent(){
    return this->event;
  }

  bool AlarmController::silence(AlarmEvent event, uint32_t duration) {
        silentDurations[event] = duration;
        // Stop any ongoing triggering
        triggerDurations[event] = 0;
        alarmHardware.stopAlarm(event);
        return true;
  }


    // TODO: this should be an interrupt
  void AlarmController::bistableAlarmTimer() {
     timer.update();
     uint32_t diff = timer.diff();
    //Debug("Alarm mod: ");
    //DebugLn(timer.elapsed() % alarmPeriod);

    if (timer.elapsed() % alarmPeriod <= 20) {
        if (!alarmLatch){
            alarmOn = !alarmOn;
            alarmLatch = true;
        }
    }
    else {
        alarmLatch = false;
    }

    // Check input devices on hardware for silence event ( for testing )
    for (uint8_t i = 1; i < durLength; i++) {
      // for now, input being 1 means we should silence for 5 seconds
      if (alarmHardware.readInput((AlarmEvent) i) == 1 && silentDurations[i] == 0) {
        silence((AlarmEvent) i, 5000);

      }
    }
	

	
    // if the time difference is non trivial
    if (diff > 0) {
        // tick every silence timer if it's nonzero
      for (uint8_t i = 0; i < durLength; i++) {
          if (silentDurations[i] > 0) {
            // ensure no underflow
            if (diff > silentDurations[i]) { 
                silentDurations[i] = 0;
            } else {
                silentDurations[i] -= diff; 
            }
          }
          if (triggerDurations[i] > 0) {
            if (diff >= triggerDurations[i]) { 
            // ensure no underflow by setting to 0
                triggerDurations[i] = 0;
                // disable alarm on hardware if duration is 0
                alarmHardware.stopAlarm((AlarmEvent) i);
            } else {
                DebugLn("Trigger duration remaining: ");
                DebugLn(triggerDurations[i]);
                triggerDurations[i] -= diff; 
            }
          }
      
      } 
    }
    timer.setLast(timer.elapsed());
  }
  bool AlarmController::trigger(AlarmEvent event, uint32_t time, uint32_t duration) {
    // setState(state); change state?
    // cannot trigger alarm if it's silenced
    if (silentDurations[event] <= 0) {
        DebugLn("Alarm Triggering");
        triggerDurations[event] = duration;
        // start alarm on hardware
        alarmHardware.triggerAlarm(event, High);
        return true;
    } else {
        DebugLn("Alarm can't be triggered because it's silenced for");
        DebugLn(silentDurations[event]);
        return false;
    }
  }

  ////////// EXTERNAL API ///////////

  // Call once to setup the alarm module.
  bool AlarmModule::setup(){
    if (ac.setState(Ok)){
        // Handle error
        Debug<const char*>("Error starting alarms!");
        return false;
    }
    DebugLn<const char*>("Alarm module setup");
    return true;
  }

  // Equivalent to Arduino loop function
  bool AlarmModule::run(uint32_t msNow){
    ac.bistableAlarmTimer();
    return true;
  }
  
  
  // Silence an alarm event.
  bool AlarmModule::silence(AlarmEvent event, uint32_t msNow, uint32_t silentDuration) {
    AlarmRecord ar(event, msNow, silentDuration);
    ac.addRecord(ar);    
    return ac.silence(event, silentDuration);
  }

  // Trigger an alarm event.
  bool AlarmModule::trigger(AlarmEvent event, uint32_t msNow, uint32_t duration) {
      AlarmRecord ar(event, msNow, duration);
      ac.addRecord(ar);    
      return ac.trigger(event, msNow, duration);
  }


  // Reset all alarms.
  bool AlarmModule::reset(){
    
    return true;
  }

}

/*
AlarmState AlarmAPI::silence(AlarmEvent event, uint32_t ms) {
    // TODO: set a timer for the event that triggered the alarm
    this->event = event;
    return this->state = SILENCED;
  }

  AlarmRecord AlarmAPI::trigger(AlarmEvent event, uint32_t time, uint32_t duration){
    AlarmRecord ar(event, time, duration);
    // Do something...
    return ar;
  }
  */
