/**
 * @file alarm_hardware.h
 * @brief Hardware controller for instance of VentOS
 * @version 0.1
 * @date 2021-03-15
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <inttypes.h>
#include <alarm_data.h>
#ifndef ALARM_HARDWARE_H
#define ALARM_HARDWARE_H
namespace VentOS_Alarm {
    /**
     * @brief Controls hardware inputs and outputs for alarms on VentOS instance
     * 
     */
    class AlarmHardware {
        public:
            /**
             * @brief Construct a new Alarm Hardware object. Initializes all necessary pins as output
             * 
             */
            AlarmHardware();
            /**
             * @brief Trigger hardware output corresponding to alarm event
             * 
             * @param event event to trigger 
             * @param state state of event 
             */
            void triggerAlarm(AlarmEvent event, AlarmState state);

            void stopAlarm(AlarmEvent event);

			uint8_t readInput(AlarmEvent event);
        private:
            /**
             * @brief Sets a given pin as output. Used for LED in demo.
             * 
             * @param led 
             */
            void setPins(int led);

            void setInPins(int led);
    };

}

#endif
