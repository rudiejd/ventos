/**
 * @file alarm_data.h
 * @brief Dictates important features of alarms on VentOS instance. This is an example file, and a new one will be generate for each independent configuration.
 * @date 2021-03-15
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef ALARM_DATA_H
#define ALARM_DATA_H

#define NUM_ALARM_RECORDS 10
#define NUM_EVENTS 8
/**
 * @brief The state of an alarm. This is currently unused, but could have some purpose going forward
 * 
 */
enum AlarmState {
    Silenced,
    Ok,
    Low,
    Medium,
    High,
    Error
};

/**
 * @brief These events currently correspond to the pins from which the alarm LEDs read
 * 
 */
enum AlarmEvent {
    None = 0,
    VentInvalid = 1,
    VentSuccess = 2,
    VentCriticalPCV = 3,
    HIGH_PRESSURE = 4,
    LOW_PRESSURE = 5,
    HIGH_FLOW = 6,
    LOW_FLOW = 7
};
#endif