#ifndef ALARM_H
#define ALARM_H

#include <inttypes.h>
#include <alarm_data.h>
#include <alarm_hardware.h>
#include <task.h>
#include <timer.h>
#ifdef ARDUINO
#include <Arduino.h>
#endif

using namespace VentOS;

namespace VentOS_Alarm {

  struct AlarmRecord {
    AlarmEvent alarmEvent = None;
    uint32_t triggerTime = 0;
    uint32_t triggerDuration = 0;
    AlarmRecord(){}
    AlarmRecord(AlarmEvent ae, uint32_t trigger, uint32_t duration) {
      alarmEvent = ae;
      triggerTime = trigger;
      triggerDuration = duration;
    }
    // message?
  };

  struct AlarmLimits {
    uint8_t min;
    uint8_t max;
    const char* message;
  };

  // Should this come from the generated struct?
  struct AlarmDefinitions {
    AlarmLimits pressure;
    AlarmLimits volume;
    AlarmLimits flow;
    AlarmLimits oxygen;
    AlarmLimits co2;
  };

  /**
    @brief The Alarm Controller is an internal class that does the
    heavy lifting of the alarm module.
  */
  class AlarmController {
    private:
      AlarmState state;
      AlarmEvent event;
      AlarmHardware alarmHardware;
      uint8_t arIndex;
      uint8_t arLength;
      AlarmRecord alarmRecords[NUM_ALARM_RECORDS];
      Timer timer;
      bool alarmOn;
      bool alarmLatch;
      uint16_t alarmPeriod;
      uint32_t silentDurations[NUM_EVENTS];
      uint32_t triggerDurations[NUM_EVENTS];
      uint8_t durLength;
    public:
      AlarmController() {
        state = Ok;
        event = None;
        arIndex = 0;
        arLength = sizeof(alarmRecords)/sizeof(alarmRecords[0]);
        alarmOn = false;
        alarmLatch = false;
        alarmPeriod = 1000;
        durLength = NUM_EVENTS;
        // initialize all silent tickers to 0
        for (uint8_t i = 0; i < NUM_EVENTS; i++) {
            silentDurations[i] = 0;
        }
        for (uint8_t i = 0; i < NUM_EVENTS; i++) {
            triggerDurations[i] = 0;
        }
      }
      //AlarmState resetState();
      bool silence(AlarmEvent event, uint32_t ms);
      bool trigger(AlarmEvent event, uint32_t time, uint32_t duration);
      bool addRecord(AlarmRecord ar);
      AlarmRecord getRecord(int i);
      AlarmState setState(AlarmState state);
      AlarmState getState();
      AlarmEvent setEvent(AlarmEvent event);
      AlarmEvent getEvent();
      void bistableAlarmTimer();
      //bool update(uint32_t msNow);
  };


  /**
    @brief The Alarm Module is the API. It encapsulates the entire alarm functionality.
  */
  class AlarmModule : public Task {
    private:
      AlarmController ac;
    public:
      AlarmModule(){
      }
      bool setup();
      bool run(uint32_t msNow);
      bool silence(AlarmEvent event, uint32_t msNow,  uint32_t silentDuration);
      bool trigger(AlarmEvent event, uint32_t msNow, uint32_t duration); // Maybe this needs to be in UTC?
      bool reset();
  };

}

#endif
