/* =====================================================================================
MIT License

Copyright (c) 2020 Public Invention

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

 *
 * Filename:  vos_simulator.h
 *
 * Description:
 *
 * Version:  1.0
 * Created:  09/28/2020 14:53:43
 * Revision:  none
 * Compiler:  gcc
 *
 * Author:  Unni Nair unnivsn@gmail.com
 * Organization:  Public Invention
 * License:  MIT
 *
 * Please see bottom of	this file for few design notes and usage suggestions.
 * (this code turned messy after some of the anticipated features were turned off)
 * =====================================================================================
 */

#ifndef __VOS_SIMULATOR_H__
#define __VOS_SIMULATOR_H__


#include <vos_sim_config.h>
#include "vos_config_mock.h"


class VosSimulator {
public:
#if 0
	VosSimulator(VosSimulatorConfig &conf): cfg(conf) {}
	VosSimulatorConfig &cfg;
 	virtual int getReading()= 0;
#endif
};

class VosPressureSimulator: public VosSimulator {
	int curr_count;
public:
	// keep this static to see where requirement is heading
 	static unsigned int getMockReading(struct volatile_ventilator_state *vent_state, bool normal);

#if 0
	VosPressureSimulator(VosSimulatorConfig &conf): VosSimulator(conf) {
		this.curr_count= -1;
	}

// keep this for a while as there is no proper over-all design right now
// remove the class itself if no requirement comes up in the near future.

 	int getReading();
 	int increasePressure(int inc= 5);
 	int decreasePressure(int dec= 5);

	//unsigned long simulatedPressureReading(bool insp);

#endif
};

#endif	//	__VOS_SIMULATOR_H__


/*
 Note on Simulator design & usage.

 All simulators are expected to use VosSimulator as the base class.
 The getReading method has to be implemented in the derived class so that
     it will return the desired reading.
 Sub-classes derived from VosSimulator are expected to provide continuous
     data values; if necessory appropriate methods may be added to sub-class
     to simulate other values.

 Note that the simulator readings are tightly controlled via config object.
 Currently pressure simulator config alone is defined
 Here are the values used in pressure simulator config
  (below text is copied from vos_sim_config.h)
  const int abs_min= 0;		// absolute minimum pressure, can never be changed
				   at run-time (change the value here if absolutely necessary)
  const int abs_max= 250;	// absolute maximum pressure, can never be
                                   changed at run-time
  int run_low;			// minimum expected pressue, can be modified
                                   while running (expected to control via feed-back loop)
  int run_up;			// maximum expected pressue during run time,
                                   can be modified while running (control via feed-back)
  int delta;		// delta value, maximum fluctuation allowed below run_up
                           (and above run_low); configured during system configuration
  int period;		// number of samples per cycle; say 300 samples for 3 sec cycle
  int high_period;	// number of samples per cycle that supply high pressure
                        (rest will be min pressure); say 100 samples out of 300 (value in period)

 These values may be set either during config object creation or by modifying config object state
 Two fields above require special mention.
 period: this is the number of data points collected in a given period,
         set this to 100 if that many samples required in one cycle.
 high_period: indicates how long (how many samples) high pressure (i.e., UP value) is
              maintained in one cycle, rest of the values in given period (say, 70 'low' values
              if high_period=30, i.e., 100-30=70) will supply low pressure values.

 Notes on pressue simulator: term 'up' corresponds to pressure maintained during breathing-in and 'low' the pressure maintained during breathing-out phases of of breathing. Breathing-out phase is expected to be longer. There is a max-limit lungs can with-stand the 'up' pressure, hence the use of abs_max variable in config. 'up' pressure can be dynamically adjusted at run-time in steps (by default 5 units) but it can never go beyond abs_max and abs_min.

 Follow these steps to use this pressure simulator object in other modules:
 	0. include vos_simulator.h in the source file, where simulator is used.
 	1. Create a config, VosSimulatorConfig, object - set appropriate run-time values.
 	2. Create VosPressureSimulator, supplying the config object to the constuctor.
 	3. Call getReading() to start collecting simulated readings.
 	4. call getMockReading(bool=true for up) to simulate 'up' or 'low' values.
 	Note that getMockReading() returns an unsigned value where as getReading() a signed int.
 Also note that simulator does not keep track of any internal clock.
 */
