#ifndef ARDUINO
#include <chrono>
#include <iostream>
#else
#include <Arduino.h>
#endif
#include <debug.h>
#include <scheduler.h>
#include <task.h>
#include <timer.h>

namespace VentOS {

// Trigger is set true to enable the next task
// to be run on the next Arduino loop() call
volatile bool trigger = false;

// This is a hardware timer interrupt for ESP32 only
#ifdef ESP32HW
// Thanks to: https://diyprojects.io/esp32-timers-alarms-interrupts-arduino-code
//int totalInterrupts;
hw_timer_t * hwTimer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

// Critical code that runs in RAM of the ESP32
void IRAM_ATTR onTime() {
    portENTER_CRITICAL_ISR(&timerMux);
    trigger = true;    
    portEXIT_CRITICAL_ISR(&timerMux);    
}
#endif

bool TaskScheduler::addTask(int index, Task *task){ 
    Debug<const char*>("addTask: ");
    DebugLn<int>(index);
    queue[index] = task;
    return true;
}

int TaskScheduler::setTimeslice(int t){ //ms
    return timeslice = t;
}

int TaskScheduler::setSchedulerMode(int mode){
    return schMode = mode;
}

bool TaskScheduler::runNextTask(){
    // qIndex is initially -1, so it skips the first task suspend.
    qIndex++;
    
    if (qIndex >= NUM_TASKS){
        qIndex = 0;
    }

    if (qIndex >= 0){ // && < NUM_TASKS
        #ifdef ESP32HW
        queue[qIndex]->run(hwTimer.elapsed());
        #else
        queue[qIndex]->run(swTimer.elapsed());
        #endif
    }

    //Debug<const char*>("qIndex: ");
    //DebugLn<int>(qIndex);
    
    #ifdef ESP32HW
    DebugLn<int>(hwTimer.elapsed());
    #else
    Debug<const char*>("swTimer: ");
    DebugLn<uint32_t>(swTimer.elapsed());
    #endif

    return true;
}

/* Called every loop() by main.cpp */
bool TaskScheduler::loop(){

    if (schMode == 0) {   
        #ifndef ESP32HW
        swTimer.update();
        #endif
        runNextTask();
    }
    else if (schMode == 1) {
        #ifndef ESP32HW // default embedded software timer
        if (swTimer.elapsed() % timeslice >= (timeslice-1)){ // TODO: fix this timing!
            trigger = true;
        }
        #endif

        if (trigger) {
            #ifdef ESP32HW
            portENTER_CRITICAL(&timerMux);
            trigger = false;
            portEXIT_CRITICAL(&timerMux);
            #else
            trigger = false;
            #endif

            runNextTask();
        }
    }
    else {
        DebugLn("Scheduler mode not valid!");
        return false;
    }

    return true;
}

bool TaskScheduler::start(){
    DebugLn<const char*>("Starting task scheduler");

    #ifdef ESP32HW
    // This is an interrupt driven sempaphore that controls access 
    //  to the task scheduler for the ESP32 environment only.

    // Configure Prescaler to 80, as our timer runs @ 80Mhz
    // Giving an output of 80,000,000 / 80 = 1,000,000 ticks / second
    hwTimer = timerBegin(0, 80, true);   
    timerAttachInterrupt(hwTimer, &onTime, true); // onTime is called on interrupt
    
    // Fire interrupt every fraction of a second (1s = 1 million ticks)
    // timeslice must be set before calling start!
    timerAlarmWrite(hwTimer, 1000*timeslice, true);
    timerAlarmEnable(hwTimer);
    #else

    #endif

    // Call setup functions
    for (int i = 0; i < queueLength; i++){
        queue[i]->setup();
    }

    return true;
}

}