#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <task.h>
#include <inttypes.h>
#include <timer.h>

namespace VentOS
{

#define NUM_TASKS 1 // This should be replaced so it's not a define

//void addTask(int index, Task *task);

class TaskScheduler {
    private:
        uint32_t timeslice; // used when schMode == 1
        int qIndex;
        int queueLength;
        int schMode; // 0 = run as fast as possible, 1 = fixed timeslice
    public:
        Timer swTimer;
        Task* queue[NUM_TASKS];
        TaskScheduler() { // TODO: add num_tasks to the constructor
            #ifdef ARDUINO
                #ifndef ESP32
                swTimer = Timer(millis()); // default embedded software timer
                #endif
            #else
            swTimer = Timer(timeSinceEpochMs()); // native software timer
            #endif

            qIndex = -1;
            queueLength = sizeof(queue)/sizeof(queue[0]);
            schMode = 0;
        }

        // TODO: these should be unit tested
        bool addTask(int index, Task *task);
        bool start();
        bool loop();
        bool runNextTask();
        int setSchedulerMode(int mode);
        int setTimeslice(int t);
};

}

#endif