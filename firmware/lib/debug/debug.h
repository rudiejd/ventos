#ifndef DEBUG_H
#define DEBUG_H

#ifdef ARDUINO
#include <Arduino.h>
#else
#include <iostream>
#include <logger.h>
#endif

namespace VentOS {

  // For example, call Debug<const char*>("Some text") or Debug<bool>(myBoolVar)
  // to get a debug output on Arduino or native environments
  template <class myType>
  inline void Debug (myType a) {
  #ifdef ARDUINO
    Serial.print(a);
  #else
    std::cout << a;
    log(a);
  #endif
  }

  // The same as Debug but adds a line break.
  template <class myType>
  inline void DebugLn (myType a) {
  #ifdef ARDUINO
    Serial.println(a);
  #else
    std::cout << a << std::endl;
    log(a);
  #endif
  }
}

#endif
