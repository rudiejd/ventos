#include <display.h>
#include <PIRDS.h>
#include <task.h>
#include <debug.h>

namespace VentOS {

    bool DisplayController::setup(){
        DebugLn<const char*>("Display controller setup");
        return true;
    }

    bool DisplayController::run(uint32_t msNow){
        DebugLn<const char*>("Display controller run");
        return true;
    }

    bool DisplayController::connect(){
        
        return true;
    }
    
    bool DisplayController::sendMessage(Message m){
        
        return true;
    }

    // Send PIRDS measurement to display.
    bool DisplayController::sendMeasurement(Measurement m){
        
        return true;
    }


}