# VentOS

The VentOS project mission is:

> To create a free and open-source software library and embedded operating system
> to enable engineering teams to develop safe and effective invasive and
> non-invasive ventilators for diverse contexts.

The project aims to:

* support the [100's of global hardware
projects](https://docs.google.com/spreadsheets/d/1inYw5H4RiL0AC_J9vPWzJxXCdlkMLPBRdPgEVKF8DZw/edit?usp=sharing)
that started when world health authorities projected a ventilator short-fall
* help the members of these teams see the most value from the software
engineering work already done within these projects

The project was initiated by [Dr Erich Schulz](https://twitter.com/ErichSchulz),
an Australian Anaesthetist together with
Dr Robert Read, PhD, of [Public Invention](https://www.pubinv.org/).
They were rapidly joined by Ben Coombs, a New Zealand Engineer, and then by Brittany Tran as
ambassador.

The core team is focused on producing high quality software products, starting
with simple, rapidly achievable modules before laying foundations for advanced
systems that will make modern ICU specialist knowledge accessible to
disadvantaged communities with over-run health care systems.

We could use your help! Please drop by the [#project-ventos channel in the Helpful Engineering slack](https://helpfulengineering.slack.com/archives/C016J0YEL7P)
and tell us a bit about yourself and how you'd like to help.

If you have been helping one of the many existing ventilator teams we'd especially love your support.

We have weekly team meetings every Tuesday 5pm PST. Please contact us on the Helpful Engineering Slack if you'd like to attend.

# Developer Guide

## Quickstart
- Docker and Git are the minimum requirements to start editing and testing the code.
- Once you have installed Docker and Git, clone the entire VentOS repo. Then create a new local branch for your own work. When you have code to contribute, simply push it to the VentOS repo and submit a Merge Request.
- You can use the terminal commands such as `make docker-test-native` to run unit tests or `make docker-bash` to start an interactive shell. There are many more commands in the makefile for you to try!

## Project Outline
- This project uses C, C++ and Python.
- PlatformIO is used for the firmware development and unit testing.
- The Arduino library is currently used to maximise compatability with as many microcontrollers as possible.
- The Docker container running Ubuntu contains all the prerequisites for local and CI testing.
- The makefile contains commands to run tests and interactive shells.
- Optionally you can setup a local development environment - the easiest way would be to create an Ubuntu Virtual Box and copy the commands from the Docker file after the RUN command, which will install all the required tools.
- Due to minor differences in Mac OSX, Windows and Linux, only the Docker environment is officially supported.

## QARA Documentation
Comprehensive QARA Documentation is available for this project [here](https://gitlab.com/project-ventos/ventos-qara).

# The Data Dictionary

This project stores as much meta data as possible within human editable, and machine readable YAML files.

See the [Data Dictionary README](/datadictionary/README.md) for more details.

# Developer Guide

## Quickstart
- Docker and Git are the minimum requirements to start editing and testing the code.
- Once you have installed Docker and Git, clone the entire VentOS repo. Then create a new local branch for your own work. When you have code to contribute, simply push it to the VentOS repo and submit a Merge Request.
- You can use the terminal commands such as `make docker-test-native` to run unit tests or `make docker-bash` to start an interactive shell. There are many more commands in the makefile for you to try!

## Project Outline
- This project uses C, C++ and Python.
- PlatformIO is used for the firmware development and unit testing.
- The Arduino library is currently used to maximise compatability with as many microcontrollers as possible.
- The Docker container running Ubuntu contains all the prerequisites for local and CI testing.
- The makefile contains commands to run tests and interactive shells.
- Optionally you can setup a local development environment - the easiest way would be to create an Ubuntu Virtual Box and copy the commands from the Docker file after the RUN command, which will install all the required tools.
- Due to minor differences in Mac OSX, Windows and Linux, only the Docker environment is officially supported.

## QARA Documentation
Comprehensive QARA Documentation is available for this project [here](https://gitlab.com/project-ventos/ventos-qara).

# The Data Dictionary
This project stores as much meta data as possible within human editable, and machine readable YAML files.

See the [Data Dictionary README](/datadictionary/README.md) for more details.

# Detailed Guide

## Tools
You will need to install Git and Docker. There are many online guides but below is a short getting started guide. If you get stuck, reach out to one of the VentOS team members for help!

## Workflow
The established workflow uses the terminal (eg. zsh, bash, Powershell) to do most of the work. This allows for automation and consistency across the developers workstations and environments.

The VentOS native (desktop) code runs on Ubuntu inside a Docker container (a virtual image). 

## How to Install Git

You can find install guides and documentation online at https://git-scm.com/
### Mac OSX
Mac OSX includes it's own version of Git, which you can't use for development! First you need to install the [Homebrew](https://brew.sh/) package manager, and then using Homebrew, install Git.
1. First check what version of Git you have installed. Open terminal and type: `git --version`
1. If the version has the word Apple, eg. (git version 2.24.3 (Apple Git-128)), you will first need to install Homebrew:
  1. Open terminal, then copy and paste the command: `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`
  1. When Homebrew finishes installing, you can then install git with the command: `brew install git`
  1. Check the Git version again: `git --version`. The terminal should output the version without the word Apple, for example: git version 2.29.2
  1. If the word Apple is still displaying, then please contact one of the VentOS team members for help.

### Windows 10
Download the exe from https://git-scm.com/download/win and use the installer.

## Ubuntu
Open terminal and type: `apt-get install git`

## How to Install Docker
Docker is a lightweight virtual environment that standardizes the testing environment to make development easier. Docker desktop can be downloaded from https://www.docker.com/. All you need to do is download and install Docker Desktop - it will run in the background. During development, Docker is called automatically through the makefile commands. You may familiarize yourself with Docker, but it is not required.

## The Makefile
The makefile contains commands which can easily be run in the terminal. The only commands you need to call during development are `make the-command-you-want`. You do not need to directly call docker or platformio at any point!

You must be in the root directory of VentOS, ie. not in any sub directory. Open the makefile to see what commands are available. You can add your own commands if you are comfortable doing so, or submit an issue to get another team member to add a command you think would be helpful during development.

## Unit Testing
To unit test test your code, use the command `make docker-test-native`.

## File Structure
PlatformIO requires a certain file and directory structure to be followed. This ensures the code is separated and easy to unit test.

Every module should have it's own folder in the lib directory, with a cpp and h file with the same name as the folder. Unit tests should be added to a folder of the same name under the tests directory. Please use an existing lib and test folder for reference to formating.

## Continuous Integration
GitLab runs a Continuous Integration (CI) pipeline that runs some linting (static file checking) and unit tests. This is done using the same Docker container you are testing on your desktop. If the CI pipeline fails, your code will not be commited!

The CI pipeline is configured in the .gitlab-ci.yml file.

## Adding Code to the Repo
You should create a new local git branch where you add your own code. Commit your code to the branch, the push the local branch to the GitLab repo. The branch will now show up in the GitLab web interface. On GitLab, open a Merge Request and please make sure to add details on what the commit is and the functionality it adds to the code base. Once a Merge Request is opened, it will be added to the next weekly meeting agenda for discussion. If accepted, it will be merged with the master branch.

The essential git commands are listed below. You should be working on your own branch unless otherwise previously organized.

**To create a new branch:**  
git branch my_new_branch  

**To add, commit and push your changes:**  
git add myfile.cpp  
git add anotherfile.cpp  
git commit -m "A good description of my commit"  
git push

**To change to a branch and download changes:**  
git checkout some_other_branch  
git pull  

**If you made a local change and want to checkout a new branch, first you will need to stash your changes:**  
git stash  

There are rules preventing any code to be committed directly to the Master branch.

Please use a descriptive name for your branch, although it can be changed later. For example: dev_flow_sensor, jane_concept_class

## IDEs and Text Editors
The VentOS workflow is setup to be editor agnostic so developers can use their favorite tools - you can use notepad, Emacs, Vi, Visual Studio etc. Classic embedded evironments such as Eclipse have been evaluated but are superceeded by PlatformIO.

If you do not have a preference, Visual Studio Code (VSCode) is recommended as a free, lightweight IDE. For VentOS, please use the terminal commands and not the VSCode plugins for compatability.

## PlatformIO
[PlatformIO](https://platformio.org/) is a Python based, modern embedded development tool. It supports features normally not seen in embedded development such as Unit Testing.

A local installation of PlatformIO is not required to contribute to VentOS, but it can be fun to learn more about it and use it in your personal Arduino projects. VSCode has a PlatformIO plugin, however, please use the terminal installation if you wish to test VentOS on an embedded device such as an Arduino Uno or Espressif ESP32.

The important PlatformIO file in the repo is platform.ini. In this file you can configure the native and embedded builds, called 'environments'. These have been setup for native, Uno and ESP32. You may add more environments, or submit as issue to request a team member to setup a new environment.

# Quick links

* [Issue queue](https://gitlab.com/project-ventos/ventos/-/boards)
* [Main repo](https://gitlab.com/project-ventos/ventos)
* [Positions vacant](https://docs.google.com/document/d/1zuPdXqJ_gFg4drvJkdByst1vagz60usFGL3S3l_cO4A/edit?usp=sharing)
* [Google drive repository](https://drive.google.com/drive/folders/1J8Fif-vDbYYBjxd8U4JiQE60yq95Uwph?usp=sharing)
* [Reference architecture](https://docs.google.com/drawings/d/1J8FYMhqwhvid358GvBmn8drd6KNj1eeb-EnO9oRORWo/edit?usp=sharing)
* [MVP](https://docs.google.com/document/d/1v2MQy5ih5gV3CdS7f8LQ4jcDB6tvXFuMgJMQLZI2IU8/edit?usp=sharing), [Vision](https://docs.google.com/document/d/1a1-D-5QBzWsPvbxSIsIBy7B0fY2WcE_DE7zi0DKQzbo/edit?usp=sharing), [Scientific grounding](https://docs.google.com/document/d/19AOx0UYfzyeG9CgVjY7wSBCTmPmhTxsHXnhCDoAK17c/edit?usp=sharing)
* [Quality and Regulatory Assurance plan](https://docs.google.com/document/d/1CxucNsA941-15sHi9XGGHucl-KbmTm_9dTJieF9CfKw/edit?usp=sharing)
* [Risk matrix](https://docs.google.com/spreadsheets/d/1YioktHK2h7lGrv7X6eUbsRB_93oLSTKpL445zBSWg0o/edit?usp=sharing)

